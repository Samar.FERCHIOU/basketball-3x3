package basket;

/**
 * The MovementStrategy interface defines a strategy for player movement in a basketball match.
 * Classes implementing this interface provide specific behavior for how a player should move during a match.
 */
public interface MovementStrategy {
	
	void move(Player player, Match match);

}
