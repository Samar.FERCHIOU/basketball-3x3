package basket;

/**
 * The MoveTowardsBasketStrategy class implements the `MovementStrategy` interface
 * to define a strategy where a player moves towards the basketball basket within a basketball match.
 */
public class MoveTowardsBasketStrategy implements MovementStrategy {

    /**
     * Moves the player towards the basketball basket within the given match context.
     *
     * @param player The player to move.
     * @param match  The basketball match context in which the movement occurs.
     */
	@Override
	public void move(Player player, Match match) {
	   
		Court court = Court.getInstance();
		
	    // Calculate horizontal and vertical difference
	    int deltaH = Court.BASKET_HORIZONTAL_POSITION - player.getPosH();
	    int deltaV = Court.BASKET_VERTICAL_POSITION   - player.getPosV();

	    // Calculate the step to make
	    int stepH = Integer.compare(deltaH, 0);
	    int stepV = Integer.compare(deltaV, 0);

    	// If the movement is possible
        if (court.getPositions()[player.getPosV() + stepV][player.getPosH() + stepH] == null) 
        {	
            court.updatePlayerPosition(player, player.getPosH() + stepH, player.getPosV() + stepV);
        } 
        else 
        {
            // If it is only possible vertically
        	if (court.getPositions()[player.getPosV() + stepV][player.getPosH()] == null ) 
        	{	
        		court.updatePlayerPosition(player, player.getPosH(), player.getPosV() + stepV);
        	}
            else 
            {
            	// If it is only possible horizontally
            	 if (court.getPositions()[player.getPosV()][player.getPosH() + stepH] == null ) 
                 {
                 	
                     court.updatePlayerPosition(player, player.getPosH() + stepH, player.getPosV());
                 }  
            	else 
            	{
            		// Make a step to the right
            		if(stepH == 0 && court.getPositions()[player.getPosV()][player.getPosH() + 1] == null)
            		{
            			court.updatePlayerPosition(player ,player.getPosH() + 1 , player.getPosV());
            		}
            		else 
            		{
            			// Make a step upwards
            			if(court.getPositions()[player.getPosV() - 1][player.getPosH()] == null)
                		{
                			court.updatePlayerPosition(player, player.getPosH() , player.getPosV() - 1);
                		}
            		}				
            	}
            }
        }
   }

}
