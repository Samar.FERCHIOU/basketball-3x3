package basket;

/**
 * The FollowOpponentStrategy class implements the `MovementStrategy` interface
 * to define a strategy where a player follows a specific opponent within a basketball match.
 */
public class FollowOpponentStrategy implements MovementStrategy {

    /**
     * Moves the player to follow a specific opponent within the given match context.
     *
     * @param player The player to move.
     * @param match  The basketball match context in which the movement occurs.
     */
	@Override
	public void move(Player player, Match match) {
		// find opponent team
		Team opponentTeam; 
		if (player.getTeam() == match.getTeamA())
		   opponentTeam = match.getTeamB();
		else 
		  opponentTeam = match.getTeamA();
		
		// find the opponent to follow
		Player opponentToFollow = opponentTeam.getPlayers()[player.getId()]; 
		
        int targetH = opponentToFollow.getPosH();
        int targetV = opponentToFollow.getPosV();
        
	    // Calculate horizontal and vertical difference
	    int deltaH = targetH - player.getPosH();
	    int deltaV = targetV - player.getPosV();

	    // Calculate the step to make
	    int stepH = Integer.compare(deltaH, 0);
	    int stepV = Integer.compare(deltaV, 0);

	    // If the target is already a neighbor, do not move
	    if (Math.abs(deltaH) <= 1 && Math.abs(deltaV) <= 1) {
	    	
	        return;
	    }
	    else 
	    {
	 
	        if (Court.getInstance().getPositions()[player.getPosV() + stepV][player.getPosH() + stepH] == null) 
	        {
	        	
	            Court.getInstance().updatePlayerPosition(player, player.getPosH() + stepH, player.getPosV() + stepV);
	        } 
	        
		}
	}
	
}
