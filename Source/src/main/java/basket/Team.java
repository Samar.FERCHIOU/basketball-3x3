package basket;

/**
 * The Team class represents a basketball team in the game.
 */
public class Team {
 
	private Player[] players; 
	private String name;
	private boolean hasTheBall;
	private int score;
	
	public String getName() {
		return name;
	}
	
	public Team(String name){
		players = new Player[3]; 
		this.name=name;
		setScore(0);
        
	}
	
    /**
     * Increments the team's score by the specified number of points.
     * 
     * @param points The number of points to increment the score by.
     */
	public void incrementScore(int points)
	{
		setScore(getScore() + points);
	}
	
	public Player[] getPlayers() {
		return players; 
	}

	public boolean isHasTheBall() {
		return hasTheBall;
	}

	public void setHasTheBall(boolean hasTheBall) {
		this.hasTheBall = hasTheBall;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}	
}
