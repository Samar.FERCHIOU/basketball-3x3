package basket;

/**
 * The Observer interface defines the contract for objects that need to be notified
 * of changes in the state of a subject.
 */
public interface Observer {
	
    /**
     * This method is called by the subject to notify the observer of a state change.
     */
    void update();
}