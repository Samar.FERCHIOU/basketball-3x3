package basket;


/**
 * The Court class represents the basketball court where the game is played.
 * It manages the positions of players on the court and provides methods to
 * interact with the court, such as placing players and updating their positions.
 */
public class Court {

	private static Court instance;
    
	public static final int LENGTH = 15; // Horizontal
    public static final int WIDTH = 11;   // Vertical

    private Player[][] positions;
    
    public static final int BASKET_HORIZONTAL_POSITION = LENGTH / 2;
    public static final int BASKET_VERTICAL_POSITION = LENGTH / 24;
    
    public static final double ARC_RADIUS = WIDTH * 0.6;
    
    /**
     * Private constructor to prevent external instantiation.
     */
    private Court() {

         positions = new Player[WIDTH][LENGTH]; // w lines and l columns 
    }

    /**
     * Gets the singleton instance of the Court class.
     *
     * @return The unique instance of the Court class.
     */
    public static Court getInstance() {
        if (instance == null) {
            instance = new Court();
        }
        return instance;
    }

	public Player[][] getPositions() {
		return positions;
	}

	public void setPositions(Player[][] tab)
	{
		positions = tab;
	}
	
    /**
     * Places a player at the specified position on the court.
     *
     * @param player The player to be placed on the court.
     * @param h      The horizontal position on the court.
     * @param v      The vertical position on the court.
     */
	public void placePlayer(Player player, int h, int v) {
        positions[v][h] = player;
        player.setPosH(h);
        player.setPosV(v);
    }
	
    /**
     * Updates the position of a player on the court.
     *
     * @param player The player whose position is being updated.
     * @param newH   The new horizontal position on the court.
     * @param newV   The new vertical position on the court.
     */
	public void updatePlayerPosition(Player player, int newH, int newV)
	{
		positions[player.getPosV()][player.getPosH()] = null;
		placePlayer(player, newH, newV);
	}
	
    /**
     * Clears all player positions on the court, making it empty.
     */
    public void emptyPositions() {
        for (int i = 0; i < positions.length; i++) {
            for (int j = 0; j < positions[i].length; j++) {
                positions[i][j] = null; 
            }
        }
    }


}
