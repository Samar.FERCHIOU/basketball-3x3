package basket;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a player in a basketball game.
 */
public class Player {
	
	private String name;
	private int posH;
	private int posV;
	private Team team;
	private boolean hasTheBall;
	private int id; 

	private int successfulShots;
	private int successfulOneShots;
	private int successfulDoubleShots;

	private MovementStrategy movementStrategy;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosH() {
		return posH;
	}
	public void setPosH(int posH) {
		this.posH = posH;
	}
	public int getPosV() {
		return posV;
	}
	public void setPosV(int posV) {
		this.posV = posV;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public boolean isHasTheBall() {
		return hasTheBall;
	}
	public void setHasTheBall(boolean hasTheBall) {
		this.hasTheBall = hasTheBall;
	} 
	public int getId()
	{
		return id;
	}

	public MovementStrategy getMovementStrategy() {
		return movementStrategy;
	}
	public void setMovementStrategy(MovementStrategy movementStrategy) {
		this.movementStrategy = movementStrategy;
	}

	public int getSuccessfulShots() {
    	return successfulShots;
    }
	public int getSuccessfulOneShots() {
		return successfulOneShots;
	}
	public int getSuccessfulDoubleShots() {
		return successfulDoubleShots;
	}
	
	public Player(Team team, String name, int id){
		this.team = team;
		this.name  = name;
		hasTheBall = false;
		this.id = id; 
		this.successfulShots = 0;
		this.movementStrategy = new RandomMovementStrategy();
	}
	
	/**
     * Increments the count of successful shots and the respective type of shots.
     *
     * @param i The type of shot (1 for a one-point shot, 2 for a two-point shot).
     */
	public void incrementSuccessfulShots(int i ) {
        successfulShots++;
		if (i==1)
		   successfulOneShots++;
		else 
		   successfulDoubleShots++;     
    }
	
	/**
     * Gets the count of neighboring players from a specified team around the player's position.
     *
     * @param neighborTeam The team whose players are considered neighbors.
     * @param positions    The 2D array representing the positions of players on the basketball court.
     * @param neighbors    An array to store the neighboring players.
     * @return The count of neighboring players from the specified team.
     */
	public int getNeighbors (Team neighborTeam, Player[][] positions, Player[] neighbors) 
	{
		int neighborCount = 0;
        // Iterate through the 8 neighbors
        for (int h = posH - 1; h <= posH + 1; h++) {
            for (int v = posV - 1; v <= posV + 1; v++) {
                // Check the field borders
                if (v != posV && h != posH && v >= 0 && isInsideTheCourt(h, v)) {

                    if (positions[v][h] != null && positions[v][h].getTeam() == neighborTeam) {
                        //Add neighbor to the list
                        neighbors[neighborCount] = positions[v][h];
                        neighborCount++;
                    }
                }
            }
        }
        return neighborCount;
	}
	
	/**
     * Executes the player's action during a basketball match.
     *
     * @param match The basketball match in progress.
     */
	public void action( Match match)
	{
		// Find the opponent team
		double randomValue = BasketballGame.random.nextDouble();
		Player[] opponentNeighbors = new Player[3]; 
		Player[] teammates = new Player[2];
		
		int j = 0;
		for(int i = 0; i < 3; i++)
		{
			if (team.getPlayers()[i] != this)
				teammates[j++] = team.getPlayers()[i];
		}
	
		Team opponentTeam; 
		if (team == match.getTeamA() )
			opponentTeam = match.getTeamB();
		else 
			opponentTeam = match.getTeamA();
			
		int oppNeighborCount = getNeighbors(opponentTeam, Court.getInstance().getPositions(), opponentNeighbors);
		
		if (hasTheBall)
		{
			setMovementStrategy(new MoveTowardsBasketStrategy());
			double distanceToBasket = calculateDistance(Court.BASKET_HORIZONTAL_POSITION, Court.BASKET_VERTICAL_POSITION);
			double shootingProbability;
		
			shootingProbability = calculateShootingProbability(oppNeighborCount, distanceToBasket);
			if(randomValue <= shootingProbability) 
			{
				shootTheBall( match.getBall(), opponentTeam, oppNeighborCount, distanceToBasket, match);//1|2   --> S, F || possession ball et initPositions  
				System.out.println("Shoot");
			}
			else
			{
				randomValue = BasketballGame.random.nextDouble();
				if (randomValue < 0.5)
				{
					passTheBall(teammates, match.getBall(), opponentTeam, oppNeighborCount, opponentNeighbors);
					System.out.println("pass");
				}
				else
				{
					movementStrategy.move(this, match);
					match.getBall().setPosHBall(this.posH);
					match.getBall().setPosVBall(this.posV);
					System.out.println("MoveToward basket");	
				}
			}
		} 
		else 
		{
			if (team.isHasTheBall()) 
			{
				setMovementStrategy(new RandomMovementStrategy());
				movementStrategy.move(this, match);
				System.out.println("MoveRandomly without ball");
			}
		    else 
			{
		    	setMovementStrategy(new FollowOpponentStrategy());
		    	movementStrategy.move(this, match);
		        System.out.println("Follow");
			}
		}
	}
	
    /**
     * Calculates the shooting probability based on opponent neighbors and distance to the basket.
     *
     * @param oppNeighborCount The count of opponent neighbors.
     * @param distanceToBasket The distance to the basketball.
     * @return The calculated shooting probability.
     */
	public double calculateShootingProbability(int oppNeighborCount, double distanceToBasket)
	{
		double shootingProbability;
		
		// Define constants or parameters for tuning the calculation
	    double maxDistance = Court.ARC_RADIUS + 0.1 * Court.WIDTH; /* Maximum distance for the highest shooting probability */
	    // System.out.println(maxDistance + " " + distanceToBasket);
	    
	    int maxOppNeighborCount = 3;/* Maximum neighbor count for the highest shooting probability */
	    
	    // Check if the distance exceeds the maximum allowed distance
	    if (distanceToBasket > maxDistance) {
	        // If so, set the shooting probability to 0
	        shootingProbability = 0.0;
	    } else {
	        // Calculate normalized values
	        double normalizedDistance = Math.min(distanceToBasket / maxDistance, 1.0); // Normalize distance between 0 and 1
	        double normalizedOppNeighborCount = Math.min((double) oppNeighborCount / maxOppNeighborCount, 1.0); // Normalize neighbor count between 0 and 1

	        // Define weights for each factor
	        double distanceWeight = 0.6; /* Weight for the distance factor */
	        double neighborCountWeight = 0.4; /* Weight for the neighbor count factor */

	        // Calculate shooting probability using a weighted sum
	        shootingProbability = distanceWeight * (1 - normalizedDistance) + neighborCountWeight * (1 - normalizedOppNeighborCount);

	        // Ensure the shooting probability is within valid bounds (0 to 1)
	        shootingProbability = Math.max(0.0, Math.min(1.0, shootingProbability));
	    }
	    
	    return shootingProbability;
	}

    /**
     * Passes the ball to a teammate or handles interception by opponents.
     *
     * @param teammates            Array of teammates to pass the ball to.
     * @param ball                 The basketball to be passed.
     * @param opponentTeam         The opposing team.
     * @param oppNeighborCountP    Count of opponent neighbors of the passing player.
     * @param opponentNeighborsP   Array of opponent neighbors of the passing player.
     */
	public void passTheBall(Player[] teammates, Ball ball, Team opponentTeam,int oppNeighborCountP, Player[] opponentNeighborsP) {
        double randomValue = BasketballGame.random.nextDouble();
		Player receiver = teammates[BasketballGame.random.nextInt(2)];
		
		Player[] opponentNeighborsR = new Player[3]; 
		int oppNeighborCountR = receiver.getNeighbors(opponentTeam,Court.getInstance().getPositions(),opponentNeighborsR); 
		
		// if both players are not surrounded by opponent players then the pass is successful otherwise it has 70% chance to succeed
		if ((oppNeighborCountP + oppNeighborCountR == 0 ) || randomValue < 0.7) 
		{	
			// update the possession of the ball 
			getTheBall(receiver ,ball); 
			System.out.println("pass successfull");
        }
	   // the ball is intercepted by a defender 
	   else 
	   {
		     // Combine the two arrays into a single array
			Player[] combinedArray = new Player[oppNeighborCountP + oppNeighborCountR];
			System.arraycopy(opponentNeighborsP, 0, combinedArray, 0,oppNeighborCountP );
			System.arraycopy(opponentNeighborsR, 0, combinedArray, oppNeighborCountP , oppNeighborCountR);
			
			// Randomly choose a defender from the combined array
			Player defender = combinedArray[BasketballGame.random.nextInt(combinedArray.length)];
			// update the possession of the ball
		    getTheBall(defender,ball); 

			team.setHasTheBall(false);
			opponentTeam.setHasTheBall(true);  
			
			System.out.println("opponent intercepts pass");
	   }
	   hasTheBall = false;
    }

    /**
     * Determines whether the player is inside the shooting zone.
     *
     * @return True if the player is inside the shooting zone, false otherwise.
     */
	public boolean isInsideTheZone()
	{
		return  (calculateDistance(Court.BASKET_HORIZONTAL_POSITION, Court.BASKET_VERTICAL_POSITION) < Court.ARC_RADIUS);
	}
	

    /**
     * Moves the player out of the shooting zone until they are outside it.
     */
	public void leaveTheZone()
	{
		while(isInsideTheZone() && isInsideTheCourt(posH, posV + 1))
		{
			Court.getInstance().updatePlayerPosition(this, posH, posV + 1);	
		}	
	}
	
	/**
     * Checks if a given position is inside the basketball court.
     *
     * @param h The horizontal position.
     * @param v The vertical position.
     * @return True if the position is inside the court, false otherwise.
     */
	public boolean isInsideTheCourt(int h, int v)
	{
		return (h >= 0 && v >= 0 && h < Court.LENGTH && v < Court.WIDTH);
	}

	/**
     * Calculates the Euclidean distance between the player and a given position.
     *
     * @param h The horizontal position.
     * @param v The vertical position.
     * @return The distance between the player and the given position.
     */
	public double calculateDistance(int h, int v)
	{
	    int deltaX = posH - h;
        int deltaY = posV - v;

        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        return distance;
    }
	
	/**
     * Makes the player take possession of the ball and updates the ball's position.
     *
     * @param player The player taking possession of the ball.
     * @param ball   The basketball.
     */
	public void getTheBall(Player player, Ball ball) {
			player.setHasTheBall(true);
			ball.setPossessor(player);
			ball.setPosVBall(player.getPosV()); 
			ball.setPosHBall(player.getPosH());
	}
	
    /**
     * Calculates the success probability of a shot based on opponent neighbors and distance to the basket.
     *
     * @param oppNeighborCount The count of opponent neighbors.
     * @param distanceToBasket The distance to the basketball.
     * @return The calculated success probability.
     */
	private double calculateSuccessProbability(int oppNeighborCount, double distanceToBasket) {

		double probability = 0.7 - 0.02 * oppNeighborCount - 0.03 * distanceToBasket;

		// Ensure probability is within the valid range [0, 1]
		return Math.max(0, Math.min(1, probability));
    }

    /**
     * Handles shooting the ball, scoring, and determining possession after a shot.
     *
     * @param ball          The basketball.
     * @param opponentTeam  The opposing team.
     * @param oppNeighborCount The count of opponent neighbors.
     * @param distanceToBasket The distance to the basketball.
     * @param match         The basketball match.
     */
	public void shootTheBall(Ball ball, Team opponentTeam,  int oppNeighborCount, double distanceToBasket, Match match) {
		double successProbability = calculateSuccessProbability(oppNeighborCount, distanceToBasket);
		double randomValue = BasketballGame.random.nextDouble(); 

		if (randomValue < successProbability) // The player scores
		{
			ball.setPosHBall(Court.BASKET_HORIZONTAL_POSITION);
			ball.setPosVBall(Court.BASKET_VERTICAL_POSITION);
			
			if (isInsideTheZone())
			{
				team.incrementScore(1);
				 incrementSuccessfulShots(1);
			}
			else
			{
				team.incrementScore(2);
				incrementSuccessfulShots(2);
			}
		
			BasketballGame.pauseGame(1000,(team.getName() + " scored !"), () -> {
	            // Code to be executed after the pause
				match.initializePlayers(opponentTeam, team);
	            System.out.println(team.getName() + " scored !");
	        });
			

			System.out.println("Success");
		}
		else 
		{
			// find the player who will get the rebound
			Player newPossessor = findNewPossessor(opponentTeam);
  
			// offensive rebound -> the team scores
             if (newPossessor.getTeam() == team) 
             {
     			ball.setPosHBall(Court.BASKET_HORIZONTAL_POSITION);
    			ball.setPosVBall(Court.BASKET_VERTICAL_POSITION);
    			if (newPossessor.isInsideTheZone())
    			{
    				team.incrementScore(1);
					newPossessor.incrementSuccessfulShots(1);
    			}
    			else
    			{
    				team.incrementScore(2);
					newPossessor.incrementSuccessfulShots(2);
    			}
    		//	match.initializePlayers(opponentTeam, team); 
    			
    			BasketballGame.pauseGame(1000,(team.getName() + " scored !"), () -> {
    	            // Code to be executed after the pause
    				match.initializePlayers(opponentTeam, team);
    	            System.out.println(team.getName() + " scored !");
    	        });
    			

				System.out.println("teammate rebound and score");

			} 
            // defensive rebound -> the new possessor leaves the zone and the game continues
			else {
				hasTheBall = false;
				getTeam().setHasTheBall(false);
				newPossessor.leaveTheZone();
				newPossessor.getTeam().setHasTheBall(true);
				getTheBall(newPossessor, ball); 
				System.out.println("opponent rebound");
			}         
			
		}
		
	}

    /**
     * Finds the new possessor of the ball among all players.
     *
     * @param opponentTeam The opposing team.
     * @return The player who becomes the new possessor.
     */
	private Player findNewPossessor(Team opponentTeam) {
    
	// Create a list for all players
	List<Player> allPlayers = new ArrayList<>();
      for (Player player : team.getPlayers()) {
        allPlayers.add(player);
    }
    for (Player player : opponentTeam.getPlayers()) {
        allPlayers.add(player);
    }

    // Choose a random player from all players
    int randomIndex = BasketballGame.random.nextInt(allPlayers.size());
    Player newPossessor = allPlayers.get(randomIndex);

    return newPossessor;
    }
}
