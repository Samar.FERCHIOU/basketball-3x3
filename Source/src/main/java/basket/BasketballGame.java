package basket;

import javax.sound.sampled.*;
import javafx.scene.control.Button;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;

/**
 * The main class representing the Basketball Game application.
 * Extends the JavaFX Application class.
 */
public class BasketballGame extends Application {

	private static Pane pane;
	private static Timeline timeline; 
    private Match match; 
    private List<Text> playerTexts = new ArrayList<>();
    private Circle ballCircle;
    private Rectangle court;
    private Circle basket;
    private Circle arc;
    private BallObserver ballObserver;   
    private Label bestScorerLabel;
    private Label winningTeamLabel;
    private Label playerStatisticsLabel; 
    private boolean gameEnded = false;
    private static boolean isMuted = false;
    private static Clip backgroundMusicClip;


    public static MTRandom random = new MTRandom();
    
    /**
     * The main method to launch the application.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String[] args) {
        launch(args);
    }

    public Match getMatch()
    {
    	return match;
    } 
    
    /**
     * The entry point for the JavaFX application.
     *
     * @param primaryStage The primary stage for the application.
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Basketball Game");
        
        Team teamA = new Team("A");
        Team teamB = new Team("B");
        
        // Use TextInputDialog to get team names from the user
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Team Names");
        dialog.setHeaderText(null);
        dialog.setContentText("Enter the name for Team A:");
        dialog.showAndWait().ifPresent(name -> teamA.setName(name));

        dialog.setContentText("Enter the name for Team B:");
        dialog.showAndWait().ifPresent(name -> teamB.setName(name));
        
        // Initialize the match
        match = new Match(teamA, teamB, Court.getInstance(), new Ball());

              
        match.initializePlayers(match.getTeamA(), match.getTeamB());
        
        // Create a Pane to hold the court and players
        pane = new Pane();
        
        // Play background music
        new Thread(this::playBackgroundMusic).start();
        
        
        ballObserver = new BallObserver(this);
      
        // Draw the basketball court
        drawCourt();
        
        // Draw the arc
        drawArc();
        
        // Draw the basket
        drawBasket();
        
        // Draw players
        drawPlayers();

        // Draw the ball
        drawBall();
        
        // Draw the start button
        drawStartButton();
        
        //Draw mute button
        drawMuteButton();


        // Set up the scene and show the stage
        Scene scene = new Scene(pane, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

       
    }

    /**
     * Starts the game loop, updating the game state at regular intervals.
     */
    private void startGameLoop() {
    	
    	// It is possible to adjust the speed 
        timeline = new Timeline(new KeyFrame(Duration.seconds(0.7), event -> {
        	
        	if (!gameEnded)
        	{  
            	// Draw the end button
            	drawEndButton();
            	
        	   // Update the game state
 	           match.updateGameState();
 	      	
	            // Redraw players and the ball
	            redrawPlayersAndBall();
	
	            // Check if the match is finished
	           if (match.isMatchFinished()) {
	               endGame();
	               
	           }
        	}

	        
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        
    }

    /**
     * Ends the game, displaying winning team, player statistics, and best scorer.
     */
    private void endGame() {
        // If the game has already ended, do nothing
        if (gameEnded) {
            return;
        }

        // Set the gameEnded flag to true
        gameEnded = true;

        // Stop the game loop
        timeline.stop();
        
        // Stop the background music
        stopBackgroundMusic();
       
        // Display the winning team
        Rectangle team = new Rectangle(325, 70, 180, 40); 
        team.setFill(Color.WHITE);
        team.setStroke(Color.BLACK);
        pane.getChildren().add(team);
        
        winningTeamLabel = new Label("Winning Team : ");
        winningTeamLabel.setLayoutX(335); 
        winningTeamLabel.setLayoutY(80); 
        winningTeamLabel.setStyle("-fx-font-size: 16; -fx-fill: red; ");
        pane.getChildren().add(winningTeamLabel);
        
        Team winningTeam = match.getWinningTeam();
        winningTeamLabel.setText(winningTeamLabel.getText() + winningTeam.getName());

        // Display statistics
        Rectangle stat = new Rectangle(180, 130, 440, 160); 
        stat.setFill(Color.WHITE);
        stat.setStroke(Color.BLACK);
        pane.getChildren().add(stat);
        
        playerStatisticsLabel = new Label("Satistics: \n");
        playerStatisticsLabel.setLayoutX(190);  
        playerStatisticsLabel.setLayoutY(140);  
        playerStatisticsLabel.setStyle("-fx-font-size: 14; -fx-fill: red;");
        pane.getChildren().add(playerStatisticsLabel);
        collectPlayerStatistics() ; 
        
        Rectangle player = new Rectangle(145, 330, 510, 40); 
        player.setFill(Color.WHITE);
        player.setStroke(Color.BLACK);
        pane.getChildren().add(player);

        bestScorerLabel = new Label("Best Scorer : ");
        bestScorerLabel.setLayoutX(155); 
        bestScorerLabel.setLayoutY(340); 
        bestScorerLabel.setStyle("-fx-font-size: 16; -fx-fill: red;");
        pane.getChildren().add(bestScorerLabel);
        
        Player bestScorer = match.getBestScorer();
        updateBestScorerLabel(bestScorer);

        // remove the players from the screen
        hidePlayers();  
    }
    

    /**
     * Pauses the game for a specified duration, displaying a message during the pause.
     *
     * @param pauseDurationMillis Duration of the pause in milliseconds.
     * @param message             Message to display during the pause.
     * @param onFinished          Callback to execute after the pause.
     */
	public static void pauseGame(int pauseDurationMillis, String message, Runnable onFinished) {
		
		timeline.pause();
	    // Display text on the screen during the pause
	    Text pauseText = new Text(message);
	    pauseText.setFont(Font.font("Arial", 24));
	    pauseText.setFill(Color.BLACK);
	    pauseText.setX(350); 
	    pauseText.setY(200); 
	    pane.getChildren().add(pauseText);
	
	    // Create a PauseTransition with the specified duration
	    PauseTransition pause = new PauseTransition(Duration.millis(pauseDurationMillis));
	    pause.setOnFinished(event -> {
            // Remove the text after the pause
            pane.getChildren().remove(pauseText);
            
            // Resume the timeline
            timeline.play();
            
            // Execute the callback after the pause
            if (onFinished != null) {
                onFinished.run();
            }
        });
	
	    // Play the pause transition
	    pause.play();
	    
	}

    /**
     * Hides players and the ball from the screen.
     */
    private void hidePlayers() {
    	
    	// hide the players and the ball
        pane.getChildren().forEach(node -> {
        if (node instanceof Circle && node != arc && node != basket) {
            ((Circle) node).setVisible(false);
        }
         });

         // hide the player id
        pane.getChildren().removeAll(playerTexts);
        playerTexts.clear();

    }
    
    
    /**
     * Draws the "Start" button to begin the game.
     */
    private void drawStartButton() {
        // Create a "Start" button
        Button startButton = new Button("Start");
        startButton.setLayoutX(350); 
        startButton.setLayoutY(250);

        // Set preferred size to make the button larger
        startButton.setPrefSize(100, 50); 

        // Set font for the button text
        Font buttonFont = new Font("Arial", 18); 
        startButton.setFont(buttonFont);
        // Add an action listener to the button
        startButton.setOnAction(event -> {
            startGameLoop();
            startButton.setVisible(false); // Hide the button when clicked
        });

        // Add the button to the Pane
        pane.getChildren().add(startButton);
    }
    
    
    /**
     * Draws the "End" button to end the game.
     */ 
    private void drawEndButton() {
    	
    	Button endButton = new Button("End");
    	endButton.setLayoutX(670);
    	endButton.setLayoutY(550);
    	endButton.setPrefSize(100, 50);
    	Font buttonFont = new Font("Arial", 18);
    	endButton.setFont(buttonFont);
    	endButton.setOnAction(event -> {
    		endGame();
    		endButton.setVisible(false);
    	
    	}); // Attach event handler
    	pane.getChildren().add(endButton);

    }

    /**
     * Draws players on the court, displaying player IDs.
     */
    private void drawPlayers() {
        // Draw teamA players on the Pane
        for (Player player : match.getTeamA().getPlayers()) {
            Circle playerCircle = new Circle(20, Color.RED);
            playerCircle.setCenterX(player.getPosH() * 50+ court.getX() + 20);
            playerCircle.setCenterY(player.getPosV() * 50 + court.getY() + 20);
            pane.getChildren().add(playerCircle);
            
         // Display player ID inside the circle
            Text playerIdText = new Text(String.valueOf(player.getId()));
            playerIdText.setStyle("-fx-font-size: 13; -fx-fill: white;"); // Set font size and text color
            playerIdText.setX(player.getPosH() * 50 + court.getX() + 15);
            playerIdText.setY(player.getPosV() * 50 + court.getY() + 25);
            pane.getChildren().add(playerIdText);
            playerTexts.add(playerIdText);
        }
        
        // Draw teamB players on the Pane
        for (Player player : match.getTeamB().getPlayers()) {
            Circle playerCircle = new Circle(20, Color.BLUE);
            playerCircle.setCenterX(player.getPosH() * 50+ court.getX() + 20);
            playerCircle.setCenterY(player.getPosV() * 50 + court.getY() + 20);
            pane.getChildren().add(playerCircle);
            
            
         // Display player ID inside the circle
            Text playerIdText = new Text(String.valueOf(player.getId()));
            playerIdText.setStyle("-fx-font-size: 13; -fx-fill: white;"); // Set font size and text color
            playerIdText.setX(player.getPosH() * 50 + court.getX() + 15);
            playerIdText.setY(player.getPosV() * 50 + court.getY() + 25);
            pane.getChildren().add(playerIdText);
            playerTexts.add(playerIdText);
        }      
       
    }

    /**
     * Draws the ball on the court.
     */
    private void drawBall() {
    	
        // Draw the ball on the Pane
        ballCircle = new Circle(10, Color.ORANGE);
        ballCircle.setCenterX((match.getBall().getPosHBall()) * 50  + court.getX() + 20);
        ballCircle.setCenterY(match.getBall().getPosVBall() * 50 + court.getY() +20);
        pane.getChildren().add(ballCircle);
    }

    /**
     * Updates the ball's display on the screen.
     */
    public void updateBallDisplay() {
    Platform.runLater(() -> {
        // Delete the previous ball
        pane.getChildren().remove(ballCircle);

        // Draw the new ball position
        drawBall();
    });
    }

    /**
     * Redraws players and the ball on the court.
     */
    private void redrawPlayersAndBall() {
        // Remove existing players and ball
        pane.getChildren().removeIf(node -> node instanceof Circle && node != arc && node != basket && node != ballCircle);

        // Remove playerIdTexts
        pane.getChildren().removeAll(playerTexts);
        playerTexts.clear();
        
        // Draw players
        drawPlayers();

        drawScore();
   }


    /**
     * Draws the score display on the screen.
     */
    private void drawScore() {
        // Remove existing score box and label
        pane.getChildren().removeIf(node -> node instanceof Label);

        // Draw the score box
        Rectangle scoreBox = new Rectangle(320, 550, 200, 40); // Adjust dimensions as needed
        scoreBox.setFill(Color.WHITE);
        scoreBox.setStroke(Color.BLACK);
        pane.getChildren().add(scoreBox);

        // Draw the score labels for Team A and Team B
        Label teamALabel = new Label(match.getTeamA().getName() + "  " + match.getTeamA().getScore() + "     ");
        teamALabel.setLayoutX(330); 
        teamALabel.setLayoutY(560);
        teamALabel.setStyle("-fx-font-size: 14; -fx-text-fill: red;");

        Label teamBLabel = new Label(match.getTeamB().getScore() + "   " + match.getTeamB().getName());
        teamBLabel.setLayoutX(410); 
        teamBLabel.setLayoutY(560); 
        teamBLabel.setStyle("-fx-font-size: 14; -fx-text-fill: blue;");

        pane.getChildren().addAll(teamALabel, teamBLabel);
    }
	
    /**
     * Draws the basketball basket on the court.
     */
	private void drawBasket() {
	    // Use the provided constants to draw the basket on the Pane
	    double basketRadius = Court.LENGTH * 0.05;
	
	    // Draw the basket as a Circle on the Pane
	    basket = new Circle(
	            Court.BASKET_HORIZONTAL_POSITION * 50 + court.getX() + 10, 
	            Court.BASKET_VERTICAL_POSITION * 50 + court.getY() + 10 , 
	            basketRadius * 50
	    );
	    basket.setFill(Color.rgb(150, 150, 150));
	    basket.setStroke(Color.TRANSPARENT);
	    pane.getChildren().add(basket);
	}
	
    /**
     * Draws the 2-point arc on the court.
     */
	private void drawArc() {
	    // Use the provided constant to draw the 2-point arc on the Pane
	    double arcRadius = Court.ARC_RADIUS;
	
	    // Draw the 2-point arc on the Pane
	    arc = new Circle(
	            Court.BASKET_HORIZONTAL_POSITION *50 + court.getX() + 20, 
	            Court.BASKET_VERTICAL_POSITION * 50 + court.getY(), 
	            arcRadius * 50 , 
	            Color.rgb(200, 200, 200)
	    );
	    arc.setStroke(Color.WHITE);
	    arc.setStrokeWidth(2);
	    pane.getChildren().add(arc);
	}

    /**
     * Draws the basketball court on the Pane.
     */
	private void drawCourt() {
	    // Use the provided constants to draw the court on the Pane
	    double courtWidth = Court.LENGTH;
	    double courtHeight = Court.WIDTH;
	
	    // Draw the court on the Pane
	    court = new Rectangle(
	            20,  
	            0,  
	            courtWidth * 50,
	            courtHeight * 50
	    );
	    court.setFill(Color.rgb(150, 150, 150));
	    court.setStroke(Color.WHITE);
	    pane.getChildren().add(court);
	    
	}

    /**
     * Updates the label displaying the best scorer of the match.
     *
     * @param bestScorer The best scorer player.
     */
	private void updateBestScorerLabel(Player bestScorer) {
	    Platform.runLater(() -> {
	        // Delete previous content
	        bestScorerLabel.setText("Best Scorer : ");
	
	        // Add best player information
	        if (bestScorer != null) {
	            bestScorerLabel.setText(bestScorerLabel.getText() +
	                                    bestScorer.getName() +
	                                    "  (Two-Point Shots: " + bestScorer. getSuccessfulDoubleShots() + ") "+
	                                    "  (One-Point Shots: " + bestScorer. getSuccessfulOneShots() + ") "
	                                    );
	        } 
	    });
	}

	
    /**
     * Collects and displays player statistics at the end of the game.
     */
	private void collectPlayerStatistics() {
    StringBuilder statisticsText = new StringBuilder();

    for (Player player : match.getTeamA().getPlayers()) {
        statisticsText.append(player.getName()).append(" :   ");
        statisticsText.append("Successful Shots: ").append(player.getSuccessfulShots()).append(" :   ");
        statisticsText.append("1 point: ").append(player.getSuccessfulOneShots()).append("  &  ");
        statisticsText.append("2 point: ").append(player.getSuccessfulDoubleShots()).append("\n");
    }
    
    statisticsText.append("\n");
    for (Player player : match.getTeamB().getPlayers()) {
        statisticsText.append(player.getName()).append(" :   ");
        statisticsText.append("Successful Shots: ").append(player.getSuccessfulShots()).append(" :   ");
        statisticsText.append("1 point: ").append(player.getSuccessfulOneShots()).append("  &  ");
        statisticsText.append("2 point: ").append(player.getSuccessfulDoubleShots()).append("\n");
    }

    playerStatisticsLabel.setText(statisticsText.toString());
   }
	
	/**
	 * Plays background music.
	 */
	private void playBackgroundMusic() {
	    try {
	        if (!isMuted) {
	            // Load the audio file using a resource stream
	            InputStream audioStream = getClass().getClassLoader().getResourceAsStream("song.wav");
	            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(audioStream));
	
	            backgroundMusicClip = AudioSystem.getClip();
	            backgroundMusicClip.open(audioInputStream);
	
	            // Start playing the audio in a loop
	            backgroundMusicClip.loop(Clip.LOOP_CONTINUOUSLY);
	            
	           
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
    /**
     * Draws the "Mute" button to mute the music.
     */ 
	private void drawMuteButton() {

	    // Add a mute button
	    Button muteButton = new Button("Mute");
	    muteButton.setLayoutX(20);
	    muteButton.setLayoutY(550);
	    muteButton.setPrefSize(120, 50);
	    Font buttonFont = new Font("Arial", 18);
	    muteButton.setFont(buttonFont);

	    // Add an action listener to the mute button
	    muteButton.setOnAction(event -> {
	        Mute();
	        muteButton.setText(isMuted ? "Unmute" : "Mute");
	    });

	    pane.getChildren().add(muteButton);
	}

	/**
	 * Stops the background music.
	 */
	private void stopBackgroundMusic() {
	    try {
	        if (backgroundMusicClip != null && backgroundMusicClip.isOpen()) {
	            backgroundMusicClip.stop();
	            backgroundMusicClip.close();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
    /**
     * Mutes and unmutes the background music 
     */
    private void Mute() {
        isMuted = !isMuted;

        // If muted, stop the background music
        if (isMuted && backgroundMusicClip != null) {
            backgroundMusicClip.stop();
        } else {
            // If unmuted, play the background music
            playBackgroundMusic();
        }
    }

}



