package basket;

import java.util.ArrayList;
import java.util.List;

/**
 * The Match class represents a basketball match between two teams.
 * It keeps track of the teams, scores, court, and the ball's position.
 */
public class Match {
	
    private Team teamA; 
    private Team teamB; 
        
    private Court court;
    
    private Ball ball;

    public Team getTeamA() {
		return teamA;
	}

	public void setTeamA(Team teamA) {
		this.teamA = teamA;
	}

	public Team getTeamB() {
		return teamB;
	}

	public void setTeamB(Team teamB) {
		this.teamB = teamB;
	}
	
	public Court getCourt() {
		return court;
	}

	public void setCourt(Court court) {
		this.court = court;
	} 
	
	public Ball getBall()
	{
		return ball;
	}
	
	
	
	public Match(Team team1, Team team2, Court court, Ball ball)
	{
		teamA = team1;
		teamB = team2; 
		
		this.court = court;
		this.ball = ball;
		
		for(int i = 0; i < 3; i++)
		{
			Player player1 = new Player(team1, (team1.getName()+" Player" + i), i);
			Player player2 = new Player(team2, (team2.getName()+" Player" + i), i);
			team1.getPlayers()[i] = player1;
			team2.getPlayers()[i] = player2;
		}
	}

    /**
     * Initializes the players and their positions at the beginning of the match.
     * The player 0 from the first team has the ball initially.
     *
     * @param team1 The first team.
     * @param team2 The second team.
     */
	public void initializePlayers(Team team1 , Team team2)
	{
		System.out.println("init");
		int l =  Court.LENGTH;
		int w =  Court.WIDTH;
		int h = l / 2; // horizontal
		int v = w - 1; // vertical
		Player player;
		
		getCourt().emptyPositions();
		if(ball.getPossessor() != null)
			ball.getPossessor().setHasTheBall(false);
		team2.setHasTheBall(false);
		
	    //player 0 of team1 has the ball
		player = team1.getPlayers()[0];

		court.placePlayer(player,h , v);

		player.setHasTheBall(true);
		team1.setHasTheBall(true);
				
		ball.setPosHBall(h);
		ball.setPosVBall(v);
		ball.setPossessor(player);
		
		player = team2.getPlayers()[0];
		
		court.placePlayer(player, h,v-1);
		
		//players of each team are facing each other	
		for (int i = 1; i < 3; i++)
		{
			do 
			{
				h = BasketballGame.random.nextInt(l - 1);
				
				v = BasketballGame.random.nextInt(w - 1);
			}while ((getCourt().getPositions()[v][h] != null ) || (getCourt().getPositions()[v + 1][h] != null));
			
			player = team1.getPlayers()[i];
			court.placePlayer(player, h,v);	
			
			
			player = team2.getPlayers()[i];
			court.placePlayer(player, h,v+1);
			
		}	
	}
	
    /**
     * Updates the game state by simulating the actions of players with the ball
     * and players from the opposing team.
     */
	public void updateGameState()
	{
		Player currentPocessor = getBall().getPossessor();
		Team currentTeam = currentPocessor.getTeam();
		Player currentPlayer;
		currentPocessor.action(this);
		
		int j = 0;
		while(currentPocessor.isHasTheBall() && j < 3)
		{
			currentPlayer = currentTeam.getPlayers()[j];
			if (currentPlayer != currentPocessor)
			{
				currentPlayer.action(this);
				

			}
			j++;
		}
		if ( currentTeam == getTeamA())
			currentTeam = getTeamB();
		else
			currentTeam = getTeamA();
		
		j = 0;
		while(currentPocessor.isHasTheBall() && j < 3)
		{
			currentPlayer = currentTeam.getPlayers()[j];   
			
			currentPlayer.action(this);

			j++;
		}
	}

    /**
     * Checks if the match has finished based on the scoring rules.
     *
     * @return True if the match has finished, false otherwise.
     */
	public boolean isMatchFinished() {
	    int scoreTeamA = teamA.getScore();
	    int scoreTeamB = teamB.getScore();
	
	    // Add condition to end the game
	    return (scoreTeamA >= 21 || scoreTeamB >= 21) && Math.abs(scoreTeamA - scoreTeamB) >= 2;
	}
	
    /**
     * Determines the winning team based on the final scores.
     *
     * @return The winning team.
     */
	public Team getWinningTeam() {
	   int scoreA = teamA.getScore();
       int scoreB = teamB.getScore();

       if (scoreA > scoreB)
	       return teamA;
	    else return teamB ; 
	}

	 /**
	  * Finds the player with the highest total score (points) in the match.
	  *
	  * @return The best scorer player.
	  */
	 public Player getBestScorer() {
	    Player bestScorer = null;
	    int maxScore = -1;
	
	    // Create a list for all players
		List<Player> allPlayers = new ArrayList<>();
			for (Player player : teamA.getPlayers()) {
				allPlayers.add(player);
			}
			for (Player player : teamB.getPlayers()) {
				allPlayers.add(player);
			}
	
			for (Player player : allPlayers) {
				int totalPoints = player.getSuccessfulOneShots() + 2 * player.getSuccessfulDoubleShots();
				if (totalPoints > maxScore) {
					maxScore = totalPoints;
					bestScorer = player;
				}
			}
	       return bestScorer;
	   }
	
}
