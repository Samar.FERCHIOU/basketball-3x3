package basket;

/**
 * The BallObserver class represents an observer for changes in the Ball's state.
 * It updates the ball display in the graphic interface of the BasketballGame.
 */
public class BallObserver implements Observer {
	
    private BasketballGame basketballGame;

    /**
     * Constructs a new BallObserver and registers it with the Ball's observers list.
     *
     * @param basketballGame The BasketballGame instance associated with the observer.
     */
    public BallObserver(BasketballGame basketballGame) {
        this.basketballGame = basketballGame;
        basketballGame.getMatch().getBall().addObserver(this);
    }

    /**
     * Updates the ball display in the graphic interface of the associated BasketballGame.
     */
    @Override
    public void update() {
    	// Used to update the ball display in the graphic interface
        basketballGame.updateBallDisplay();
    }
}
