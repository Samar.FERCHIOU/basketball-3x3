package basket;
import java.util.ArrayList;
import java.util.List;

/**
 * The Ball class represents the basketball used in the game. It implements the
 * Subject interface, allowing observers to be notified of changes to the ball's
 * state, such as its position or possessor.
 */
public class Ball implements Subject {
    private Player possessor;
    private int posHBall ; 
    private int posVBall ; 
	private List<Observer> observers = new ArrayList<>();
    
	public Player getPossessor() {
		return possessor;
	}

	public void setPossessor(Player possessor) {
		this.possessor = possessor;
		this.posHBall = possessor.getPosH();
        this.posVBall = possessor.getPosV();
        notifyObservers();
	}

	public int getPosVBall() {
		return posVBall;
	}

	public void setPosVBall(int posYBall) {
		this.posVBall = posYBall;
		notifyObservers();
	}

	public int getPosHBall() {
		return posHBall;
	}

	public void setPosHBall(int posXBall) {
		this.posHBall = posXBall;
		 notifyObservers();
	} 

	@Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
