package basket;

/**
 * The RandomMovementStrategy class implements the MovementStrategy interface
 * by providing a strategy where a player moves randomly with certain probabilities
 * in different directions.
 */
public class RandomMovementStrategy implements MovementStrategy {

    /**
     * Moves the player randomly based on cumulative probabilities for different directions.
     * 
     * @param player The player to be moved.
     * @param match The current basketball match context.
     */
	@Override
	public void move(Player player, Match match) {
		// Cumulative probabilities corresponding to the steps 
		double[] directionProbability = {0.4, 0.6, 1.0}; 
		int[] step = {-1, 0, 1};
		
		double randomH;
		double randomV;
		int h;
		int v;
		
		do {
			h = 0;
			
			randomH = BasketballGame.random.nextDouble();
			while(randomH > directionProbability[h])
			{
				h++;
			}
			
			v = 0;
			
			randomV = BasketballGame.random.nextDouble();
			while(randomV > directionProbability[v])
			{
				v++;
			}

			// Check if the position is valid : empty and inside the field or the same as the actual position 
		} while (! ( player.isInsideTheCourt(player.getPosH() + step[h], player.getPosV() + step[v]) && ( (Court.getInstance().getPositions()[player.getPosV() + step[v]][player.getPosH()+ step[h]] == null) || (step[v] == 0 && step[h] == 0))));
		Court.getInstance().updatePlayerPosition(player, player.getPosH() + step[h],player.getPosV() + step[v]);

	}

}
