package basket;

/**
 * The Subject interface represents the subject in the observer pattern.
 * It defines methods for adding, removing, and notifying observers.
 */
public interface Subject {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}

