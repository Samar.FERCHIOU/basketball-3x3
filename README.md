# Simulation de Match de Basketball 3x3

## Auteurs
Ferchiou Samar & Slama Nouha

## Introduction
Bienvenue dans la simulation de match de basketball 3x3 ! 🏀 


## Configuration du Projet
Assurez-vous d'avoir Maven installé sur votre machine et d'avoir la version Java 17. <br>

## Pour construire le projet
mvn clean install

## Pour exécuter la simulation
mvn javafx:run

## Pour consulter la documentation javadoc
Ouvrir le fichier index.html dans le dossier target/apidocs généré par maven après le build.

## Remarque:
Nous espérons que le morceau de musique vous plaira 🎶.<br>
Si par hasard, vous préférez regarder en silence, n'hésitez pas à appuyer sur le bouton "Mute".<br><br>
Mais en réalité, les vrais matchs de basketball 3x3 se jouent avec une ambiance musicale continue 🎶🏀. <br><br>
Sachez que le choix de "Sidi Mansour" n'était pas une décision à la légère. <br>
On dit que même les joueurs virtuels sont plus inspirés avec cette mélodie tunisienne connue à l'échelle internationale. 
C'est "scientifiquement prouvé" ! 😄  

